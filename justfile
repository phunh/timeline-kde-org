extract:
  cd {{invocation_directory()}} && PACKAGE=websites-timeline-kde-org hugoi18n extract pot
generate:
  cd {{invocation_directory()}} && PACKAGE=websites-timeline-kde-org hugoi18n generate -k
